#include<stdio.h>

int pattern(int n);

int main()
{
	int n;

	printf("Enter the number of rows : ");
	scanf("%d", &n);

	pattern(n);

	return 0;
}

int pattern(int n)
{
	for(int i = 1 ; i <= n ; i++)
	{
		for(int j = i ; j >= 1 ; j--)
		{
			printf("%d",j);
		}
		printf("\n");
	}
}
