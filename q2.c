#include<stdio.h>

int fibonacciSeq(int n);

int main()
{
	int n;

	printf("Enter the number of rows : ");
	scanf("%d", &n);

	for(int i = 0 ; i <= n ; i++)
	{
		printf("%d \n", fibonacciSeq(i));
	}

	return 0;
}

int fibonacciSeq(int n)
{
	if( n == 0 || n == 1)
	{
		return n;
	}
	else
	{
		return fibonacciSeq(n-1) + fibonacciSeq(n-2);
	}
}
